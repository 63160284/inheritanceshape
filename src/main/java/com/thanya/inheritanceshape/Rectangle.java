/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanya.inheritanceshape;

/**
 *
 * @author Thanya
 */
public class Rectangle extends Shape{
    protected double width;
    protected double height;
    protected double area;
    public Rectangle(double width, double height){
        this.width = width;
        this.height = height;
    }
    
    @Override
    public double calArea(){
        return area = width*height;
    }
    
    @Override
    public void print(){
        System.out.println("Rectangle: width: "+width+", height: "+height
                +", area = "+area);
    }
}
