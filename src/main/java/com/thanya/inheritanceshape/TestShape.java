/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanya.inheritanceshape;

/**
 *
 * @author Thanya
 */
public class TestShape {
    public static void main(String[] args) {
        Shape shape = new Shape();
        shape.calArea();
        shape.print();
        
        Circle circle = new Circle(3);
        circle.calArea();
        circle.print();
        Circle circle2 = new Circle(4);
        circle2.calArea();
        circle2.print();
        
        Triangle triangle = new Triangle(4,3);
        triangle.calArea();
        triangle.print();
        
        Rectangle rectangle = new Rectangle(3,4);
        rectangle.calArea();
        rectangle.print();
        
        Square square = new Square(2);
        square.calArea();
        square.print();
        
        System.out.println("Circle is Shape: "+(circle instanceof Shape));
        System.out.println("Circle2 is Shape: "+(circle2 instanceof Shape));
        System.out.println("Shape is Circle: "+(shape instanceof Circle));
        System.out.println("Triangle is Shape: "+(triangle instanceof Shape));
        System.out.println("Shape is Triangle "+(shape instanceof Triangle));
        System.out.println("Rectangle is Shape: "+(rectangle instanceof Shape));
        System.out.println("Shape is Rectangle: "+(shape instanceof Rectangle));
        System.out.println("Square is Shape: "+(square instanceof Shape));
        System.out.println("Shape is Square "+(shape instanceof Square));
        System.out.println("Shape is Shape: "+(shape instanceof Shape));
        
        Shape[] shapes = {circle,circle2,triangle,rectangle,square};
        for(int i=0; i<shapes.length; i++){
            shapes[i].calArea();
            shapes[i].print();
        }
    }
}
