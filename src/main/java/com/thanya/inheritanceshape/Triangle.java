/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanya.inheritanceshape;

/**
 *
 * @author Thanya
 */
public class Triangle extends Shape{
    private double base;
    private double height;
    private double area;
    public Triangle(double base, double height){
        this.base = base;
        this.height = height;
    }
    
    @Override
    public double calArea(){
        return area = 1/2.0*base*height;
    }
    
    public void print(){
        System.out.println("Triangle: base: "+base+", height: "+height
                +", area = "+area);
    }
}
