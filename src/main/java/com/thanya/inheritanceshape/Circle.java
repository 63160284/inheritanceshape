/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanya.inheritanceshape;

/**
 *
 * @author Thanya
 */
public class Circle extends Shape{
    private double r;
    private double pi = 22.0/7;
    private double area;
    public Circle(double r){
        this.r = r;
    }
    
    @Override
    public double calArea(){
        return area = pi*r*r;
    }
    
    @Override
    public void print(){
        System.out.println("Circle: pi: 22/7, r: "+r+", area = "+area);
    }
}
