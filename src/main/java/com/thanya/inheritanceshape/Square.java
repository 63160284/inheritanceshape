/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanya.inheritanceshape;

/**
 *
 * @author Thanya
 */
public class Square extends Rectangle{
    public Square(double side){
        super(side,side);
    }
    public void print(){
        System.out.println("Square: side: "+width+", area = "+area);
    }
}
